<x-guest-layout>
    <x-authentication-card>

        <x-slot name="logo">
            <x-authentication-card-logo />
        </x-slot>
        <x-validation-errors class="mb-4" />

        @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
        @endif

        <div style="text-align: center; padding: 20px;">
            <h4 style="margin-bottom: 20px;">Facebook Login and Profile Retrieval</h4>
            <a style="display: inline-block; padding: 10px 20px; background-color: #2535a7; color: #fff; text-decoration: none; border-radius: 5px; font-size: 16px; transition: background-color 0.3s ease-in-out;" href="{{ url('auth/facebook') }}">Login With Facebook</a>
        </div>

    </x-authentication-card>
</x-guest-layout>
