# Facebook Login and Profile Retrieval

# Task

  Create a feature in a Laravel application that allows users to log in using their Facebook accounts. Additionally,  retrieve basic profile information from Facebook to either log in existing users or create new ones.

# Solution

1. **Migration:**

- Define the database structure for user information using a migration file.

2. **FacebookController:**

- Create a controller to manage the Facebook authentication process "FacebookController".

- Redirect users to the Facebook authentication page using the "facebookpage" method.

- Process the Facebook authentication callback in the "facebookredirect" method.

- Log in existing users or create new ones based on Facebook information.

3. **View:**

- Displayed profile information and email address
