<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class FacebookController extends Controller
{

    /**
     * Redirects the user to the Facebook authentication page.
     */
    public function facebookpage()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Handles Facebook authentication callback.
     * Logs in existing users or creates new ones.
     * Redirects to the intended URL, usually the profile page.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function facebookredirect()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            $findUser = User::where('facebook_id', $user->id)->first();

            if ($findUser) {
                Auth::login($findUser);
            } else {
                $newUser = User::updateOrCreate(['email' => $user->email], [
                    'name' => $user->name,
                    'facebook_id' => $user->id,
                    'password'  => encrypt('pass123')
                ]);

                Auth::login($newUser);
            }

            return redirect()->intended('user/profile');
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
